import re
import itertools
import json
import argparse
import os

SENTENCE_END_TOKENS = [".", "!", "?"]
TOKEN_SPLIT_SYMBOLS = "([–\\(\\)\\[\\]:])"
ABBREVIATIONS = ['ang.']


def preprocess_text(text: str):
    text = text.replace('\n', ' ')
    text = re.sub(' +', ' ', text)
    return text


def text_to_sentences(text: str, end_tokens):
    indicies = [i for i, x in enumerate(text) if x in end_tokens]
    correct_ending_indicies = list(filter(lambda idx: is_sentence_end(text, idx, 1), indicies))
    correct_ending_indicies.insert(0, -2)
    sentences = [text[i+2:j+1] for i, j in zip(correct_ending_indicies, correct_ending_indicies[1:]+[-2])]
    return sentences


def is_abbreviation(token):
    a = token in ABBREVIATIONS
    return a


def is_numeral(token):
    match = re.match("^\\d+\\.$", token) is not None
    return match


def is_fraction(token):
    match = re.match("^\\d+[.,]\\d+$", token) is not None
    return match


def is_initial(token):
    return re.match("[A-Z]+\\.", token) is not None


def analyze_mid_sentence_separators(token):
    if (not contains_separators(token)) or is_abbreviation(token) or is_numeral(token) or is_fraction(token) or is_initial(token):
        return [token]
    else:
        return list(re.search("(.*)([.,])", token).groups())


def contains_separators(token):
    return '.' in token or ',' in token


def sentence_to_tokens(text: str):
    tokens_with_symbols_separated = re.split(TOKEN_SPLIT_SYMBOLS, text)
    tokens_with_whitespace_removed = [t.split() for t in tokens_with_symbols_separated]
    sentence_tokens_flat = list(itertools.chain.from_iterable(tokens_with_whitespace_removed))
    tokens_with_dots_analyzed = [analyze_mid_sentence_separators(token) for token in sentence_tokens_flat]
    result = list(itertools.chain.from_iterable(tokens_with_dots_analyzed))
    return result


def is_sentence_end(text, idx, n):
    end_token = text[idx:(idx+n)]

    if end_token == '.':
        return is_sentence_end_fullstop(text, idx, n)
    elif end_token == '!':
        return is_sentence_end_exclamation(text, idx, n)
    elif end_token == '?':
        return is_sentence_end_question(text, idx, n)
    else:
        raise NotImplementedError()


def is_sentence_end_fullstop(text, idx, n) -> bool:
    if (idx + 2) < len(text) and text[idx + 2].isupper() and not text[idx-1].isupper():
        return True
    return False


def is_sentence_end_exclamation(text, idx, n) -> bool:
    return True  # TODO


def is_sentence_end_question(text, idx, n) -> bool:
    return True  # TODO


def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input-file',
        required=True,
        help='Path to file for tokenization',
        type=str,
    )
    parser.add_argument(
        '--out-dir',
        required=True,
        help='Name of directory to save generated data',
        type=str,
    )

    return parser.parse_args()

def main():
    args = get_args()

    file_path = args.input_file
    file_name = file_path.split("/")[-1].split(".")[0]
    out_dir = args.out_dir

    f = open(file_path, "r")
    text = f.read()
    text = preprocess_text(text)
    # print(text)
    sentences = text_to_sentences(text, SENTENCE_END_TOKENS)
    # for sentence in sentences:
    #     print(sentence)

    sentences_tokens = [sentence_to_tokens(sentence) for sentence in sentences]
    json_dict = {
        "sentences": {
            i: {
                j: token
                for j, token in enumerate(sentence)
            }
            for i, sentence in enumerate(sentences_tokens)
        }
    }
    # print(sentences_tokens)
    with open(os.path.join(out_dir, f'{file_name}.json'), 'w', encoding='utf8') as fp:
        json.dump(json_dict, fp, ensure_ascii=False, indent=4)


if __name__ == '__main__':
    main()
