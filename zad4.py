import json
import urllib.request
import urllib.error
import urllib.parse
import time
import abc
from zipfile import ZipFile
import os
from shutil import rmtree

import requests

from settings import KRNNT_RESULTS_UNPROCESSED_DIR, WIKI_DATA_TRAIN_FILE, WIKI_DATA_TEST_FILE, MORPHODITA_RESULTS_DIR, WCRFT2_RESULTS_DIR


class RemoteTagger(abc.ABC):
    def __init__(self, url, input_path, result_path):
        self.url = url
        self.input_path = input_path
        self.result_path = result_path

    @abc.abstractmethod
    def tag(self):
        pass


class ClarinTagger(RemoteTagger):
    def __init__(self, url, input_path, result_path, tagger):
        super().__init__(url, input_path, result_path)
        self.tagger = tagger

    def upload(self):
        with open(self.input_path, "rb") as input_file:
            doc = input_file.read()
        return urllib.request.urlopen(urllib.request.Request(self.url + '/upload/', doc, {'Content-Type': 'binary/octet-stream'})).read()

    def process(self, data):
        doc = json.dumps(data).encode()
        taskid = urllib.request.urlopen(urllib.request.Request(self.url + '/startTask/', doc, {'Content-Type': 'application/json'})).read().decode("utf-8")
        print(taskid)
        time.sleep(0.2)
        resp = urllib.request.urlopen(urllib.request.Request(self.url + '/getStatus/' + taskid))
        data = json.load(resp)
        while data["status"] == "QUEUE" or data["status"] == "PROCESSING":
            time.sleep(3)
            print(data["status"])
            resp = urllib.request.urlopen(urllib.request.Request(self.url + '/getStatus/' + taskid))
            data = json.load(resp)
        if data["status"] == "ERROR":
            print(("Error " + data["value"]))
            return None
        return data["value"]

    def tag(self):
        user = "235040@student.pwr.edu.pl"
        task = f"any2txt|{self.tagger}"
        global_time = time.time()
        fileid = self.upload().decode("utf-8")
        lpmn = 'filezip(' + fileid + ')|' + task + '|dir|makezip'
        data = {'lpmn': lpmn, 'user': user}
        data = self.process(data)
        if data is not None:
            data = data[0]["fileID"]
            content = urllib.request.urlopen(urllib.request.Request(self.url + '/download' + data)).read()
            with open(self.result_path, "wb") as outfile:
                outfile.write(content)
        print(("GLOBAL %s seconds ---" % (time.time() - global_time)))


class KrnntTagger(RemoteTagger):
    def tag(self):
        result_dir = os.path.dirname(self.result_path)
        temp_dir = os.path.join(result_dir, "temp")
        temp_results = os.path.join(result_dir, "temp_results")
        if os.path.isdir(temp_dir):
            rmtree(temp_dir)
        os.mkdir(temp_dir)

        if os.path.isdir(temp_results):
            rmtree(temp_results)
        os.mkdir(temp_results)

        with ZipFile(self.input_path, 'r') as zipObj:
            zipObj.extractall(temp_dir)

        for file_name in os.listdir(temp_dir):
            file_path = os.path.join(temp_dir, file_name)
            with open(file_path, "r") as f:
                text = f.read()
                response = requests.post(self.url, data=text.encode('utf-8'))
                response_text = response.text.encode('utf-8')
                with open(os.path.join(temp_results, file_name), "wb") as result_file:
                    result_file.write(response_text)

        with ZipFile(self.result_path, 'w') as results_zip:
            for file_name in os.listdir(temp_results):
                file_path = os.path.join(temp_results, file_name)
                with open(file_path, "r") as f:
                    results_zip.write(file_path, file_name)
        rmtree(temp_dir)
        rmtree(temp_results)


if __name__ == '__main__':
    files = [(WIKI_DATA_TEST_FILE, "wiki_test.zip"), (WIKI_DATA_TRAIN_FILE, "wiki_train.zip")]
    tagger_ctors = [
        # lambda input_path, result_name: KrnntTagger("http://localhost:9003", input_path, os.path.join(KRNNT_RESULTS_UNPROCESSED_DIR, result_name)),
        lambda input_path, result_name: ClarinTagger("http://ws.clarin-pl.eu/nlprest2/base", input_path, os.path.join(WCRFT2_RESULTS_DIR, result_name), "wcrft2"),
        lambda input_path, result_name: ClarinTagger("http://ws.clarin-pl.eu/nlprest2/base", input_path, os.path.join(MORPHODITA_RESULTS_DIR, result_name), "morphoDita"),
    ]

    for tagger_ctor in tagger_ctors:
        for input_path, result_name in files:
            tagger = tagger_ctor(input_path, result_name)
            print(tagger, tagger.input_path)
            tagger.tag()
