import requests

if __name__ == '__main__':
    f = open(f"data/test-raw.txt", "r")
    text = f.read()

    response = requests.post('http://localhost:9003', data=text.encode('utf-8'))

    response_text = response.text.encode('utf-8')
    with open("results/taggers/krnnt/unprocessed/test-raw.txt", "wb") as f:
        f.write(response_text)
