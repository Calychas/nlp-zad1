from settings import *
from lxml import etree
import argparse
from tqdm.auto import tqdm
import os


def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input-dir',
        required=True,
        help='Name of directory consisting unprocessed data',
        type=str,
    )
    parser.add_argument(
        '--out-dir',
        required=True,
        help='Name of directory to save processed data',
        type=str,
    )

    return parser.parse_args()


def process_dir_and_save(input_dir, output_dir):
    input_files = list(filter(lambda x: ".txt" in x, os.listdir(input_dir)))
    for file_name in tqdm(input_files):
        file_xml = convert_file_to_xml(os.path.join(input_dir, file_name))
        save_file(file_xml, os.path.join(output_dir, f"{file_name.split('.')[0]}.xml"))


def convert_file_to_xml(file_path):
    with open(file_path, "r") as f:
        text = f.read()
        return convert_text_to_xml(text)


def convert_text_to_xml(text):
    chunklist_xml = etree.Element("chunkList")
    chunk_xml = etree.SubElement(chunklist_xml, "chunk", {"id": "1", "type": "p"})  # FIXME: everything is one chunk

    sentences = text.split("\n\n")
    for s_idx, sentence in enumerate(sentences):
        sentence_xml = etree.SubElement(chunk_xml, "sentence", {'id': str(s_idx + 1)})
        sentence_lines = sentence.split("\n")
        words_data = list(zip(sentence_lines, sentence_lines[1:]))[::2]
        for orth, lex in words_data:
            orth_word, orth_preceding = orth.split("\t")
            lex_lem, lex_tag, _ = lex.split("\t")[1:]

            if orth_preceding == "none":
                etree.SubElement(sentence_xml, 'ns')

            tok_xml = etree.SubElement(sentence_xml, 'tok')
            orth_xml = etree.SubElement(tok_xml, 'orth')
            orth_xml.text = orth_word

            lex_xml = etree.SubElement(tok_xml, 'lex', {'disamb': '1'})
            base_xml = etree.SubElement(lex_xml, 'base')
            base_xml.text = lex_lem
            ctag_xml = etree.SubElement(lex_xml, 'ctag')
            ctag_xml.text = lex_tag

    xml_header = '<?xml version="1.0" encoding="UTF-8"?>\n' + '<!DOCTYPE chunkList SYSTEM "ccl.dtd">\n'
    xml_str = xml_header + etree.tostring(chunklist_xml, pretty_print=True, encoding="utf-8").decode("utf-8")

    return xml_str


def save_file(text, output_path):
    with open(output_path, "w") as f:
        return f.write(text)


def main():
    args = get_args()
    input_dir = args.input_dir
    output_dir = args.out_dir

    process_dir_and_save(input_dir, output_dir)


if __name__ == '__main__':
    main()
