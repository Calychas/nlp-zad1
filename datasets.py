from zipfile import ZipFile
import tempfile
import os
import xml.etree.ElementTree as ET
from sklearn.feature_extraction.text import CountVectorizer
import pickle as pkl
import pandas as pd
from settings import WIKI_DATA_TRAIN_FILE, CLASS_MAPPING_FILE, MORPHODITA_RESULTS_DIR, PROCESSED_DATA_DIR, KRNNT_RESULTS_PROCESSED_DIR, WCRFT2_RESULTS_DIR

NOUN = "noun"
VERB = "verb"
ADJ = "adj"

NOUN_TAGS = {"subst", "depr", "ger"}
VERB_TAGS = {"fin", "praet", "impt", "imps", "inf"}
ADJ_TAGS = {"adj", "adja", "adjp", "adjc", "pact", "ppas"}


tag_to_pos_mapping = {
    tag: pos
    for tag_list, pos in zip([NOUN_TAGS, VERB_TAGS, ADJ_TAGS], [NOUN, VERB, ADJ])
    for tag in tag_list
}


def create_dataset(input_zip_train_file, input_zip_test_file, output_path_train, output_path_test, tags_to_select):
    file_names = []
    corpus = []

    def get_texts_from_dir(input_zip, temp_dir, subdir_name):
        n_files = 0
        os.mkdir(os.path.join(temp_dir, subdir_name))

        with ZipFile(input_zip, 'r') as f:
            subdir = os.path.join(temp_dir, subdir_name)
            f.extractall(subdir)
            for xml_name in os.listdir(subdir):
                tree = ET.parse(os.path.join(subdir, xml_name))
                lex = tree.getroot().findall(".//lex")
                words = []
                for l in lex:
                    base = l.find("base").text
                    tag = l.find("ctag").text
                    if tag.split(":")[0] in tags_to_select:
                        words.append(base)
                file_names.append(xml_name)
                corpus.append(" ".join(words))
                n_files += 1
        return n_files

    with tempfile.TemporaryDirectory(suffix="nlp") as temp_dir:
        n_train = get_texts_from_dir(input_zip_train_file, temp_dir, "train")
        n_test = get_texts_from_dir(input_zip_test_file, temp_dir, "test")
    vectorizer = CountVectorizer()
    X = vectorizer.fit_transform(corpus).toarray()
    class_mapping = read_class_mapping()
    class_dict = dict(zip(class_mapping.name, class_mapping['class']))
    cols = vectorizer.get_feature_names()

    df_train = pd.DataFrame(index=file_names[:n_train], columns=cols, data=X[:n_train, :])
    df_train['__label__'] = df_train.index.map(lambda id: class_dict[id.split("_")[0]])
    df_train.to_pickle(output_path_train)

    df_test = pd.DataFrame(index=file_names[n_train:], columns=cols, data=X[n_train:, :])
    df_test['__label__'] = df_test.index.map(lambda id: class_dict[id.split("_")[0]])
    df_test.to_pickle(output_path_test)


def read_class_mapping():
    return pd.read_pickle(CLASS_MAPPING_FILE)


def create_class_mapping(input_zip_file, output_path):
    with ZipFile(input_zip_file, 'r') as f:
        file_name_list = f.namelist()

    prefixes = [file_name.split("_")[0] for file_name in file_name_list]
    unique_prefixes = sorted(set(prefixes))
    class_with_indices = list(enumerate(unique_prefixes))
    df = pd.DataFrame(class_with_indices, columns=['class', 'name'])
    df.to_pickle(output_path)


if __name__ == '__main__':
    types = [(NOUN, NOUN_TAGS), (VERB, VERB_TAGS), (ADJ, ADJ_TAGS)]
    taggers_with_paths = [
        ("morphodita", MORPHODITA_RESULTS_DIR),
        ("wcrft2", WCRFT2_RESULTS_DIR),
        ("krnnt", KRNNT_RESULTS_PROCESSED_DIR)
    ]

    train_file = "wiki_train.zip"
    test_file = "wiki_test.zip"

    for pos, tag_list in types:
        for tagger, tagger_path in taggers_with_paths:
                print(pos, tagger)
                input_train = os.path.join(tagger_path, train_file)
                input_test = os.path.join(tagger_path, test_file)
                output_train = os.path.join(PROCESSED_DATA_DIR, f"{pos}_{tagger}_train.pkl")
                output_test = os.path.join(PROCESSED_DATA_DIR, f"{pos}_{tagger}_test.pkl")
                print(f"\t{input_train} -> {output_train}")
                print(f"\t{input_test} -> {output_test}")
                create_dataset(input_train,
                               input_test,
                               output_train,
                               output_test,
                               tag_list)
