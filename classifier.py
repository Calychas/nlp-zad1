from settings import *
import pandas as pd
import os
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import f1_score, confusion_matrix, classification_report
from datasets import read_class_mapping
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import gc


def classify(tagger, pos):
    train_ds_name = f"{pos}_{tagger}_train.pkl"
    test_ds_name = f"{pos}_{tagger}_test.pkl"
    train_ds = pd.read_pickle(os.path.join(PROCESSED_DATA_DIR, train_ds_name))
    test_ds = pd.read_pickle(os.path.join(PROCESSED_DATA_DIR, test_ds_name))
    research_name = "_".join(train_ds_name.split("_")[:2])

    class_mapping = read_class_mapping()
    labels = list(class_mapping.iloc[:, 1])

    label_col = "__label__"
    X_train, y_train = train_ds.drop(label_col, axis=1), train_ds[label_col]
    X_test, y_test = test_ds.drop(label_col, axis=1), test_ds[label_col]

    model = MultinomialNB(fit_prior=False)
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)

    f1 = f1_score(y_test, y_pred, average='macro')

    cm = np.array(confusion_matrix(y_test, y_pred), dtype=np.int)
    fig, axes= plt.subplots(figsize=(15, 15))
    cm_chart = sns.heatmap(cm / cm.sum(axis=1, keepdims=True), cmap="RdBu_r", fmt='.2f', annot=True, annot_kws={"size": 5}, center=0,
                           square=True, xticklabels=labels, yticklabels=labels)
    cm_chart.set(xlabel='Predicted', ylabel='True')
    plt.tight_layout()
    plt.savefig(os.path.join(RESEARCH_DIR, f"cm_{research_name}.svg"), format="svg")
    # plt.show()
    return f1


if __name__ == '__main__':
    taggers = ["morphodita", "wcrft2", "krnnt"]
    poss = ["adj", "verb", "noun"]
    res = []
    for pos in poss:
        for tagger in taggers:
            print(pos, tagger)
            f1 = classify(tagger, pos)
            print(f1)
            res.append((tagger, pos, f1))
            gc.collect()
    df = pd.DataFrame(res, columns=['Tagger', 'POS', 'F1'])
    df.to_csv(os.path.join(RESEARCH_DIR, "zad4.csv"), index=False)
