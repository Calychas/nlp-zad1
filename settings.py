import os

PROJECT_DIR = os.path.normpath(os.path.dirname(__file__))

DATA_DIR = os.path.join(PROJECT_DIR, "data")

RESULTS_DIR = os.path.join(PROJECT_DIR, "results")
TAGGERS_DIR = os.path.join(RESULTS_DIR, "taggers")
KRNNT_RESULTS_DIR = os.path.join(TAGGERS_DIR, "krnnt")
KRNNT_RESULTS_UNPROCESSED_DIR = os.path.join(KRNNT_RESULTS_DIR, "unprocessed")
KRNNT_RESULTS_PROCESSED_DIR = os.path.join(KRNNT_RESULTS_DIR, "processed")
MORPHODITA_RESULTS_DIR = os.path.join(TAGGERS_DIR, "morphodita")
WCRFT2_RESULTS_DIR = os.path.join(TAGGERS_DIR, "wcrft2")

WIKI_DATA_TRAIN_FILE = os.path.join(DATA_DIR, "wiki_train_34_categories_data.zip")
WIKI_DATA_TEST_FILE = os.path.join(DATA_DIR, "wiki_test_34_categories_data.zip")

PROCESSED_DATA_DIR = os.path.join(DATA_DIR, "processed")
CLASS_MAPPING_FILE = os.path.join(PROCESSED_DATA_DIR, "class_mapping.pkl")

RESEARCH_DIR = os.path.join(RESULTS_DIR, "research")
